# OSP HEAT Ansible template

A way to deploy an app with auto-scaling, manilla file sharing, LoadBalancer with OSP 16.1 HEAT and ansible-pull for auto-configuration of virtual machines.

## Usage

```
source myopenrc
ansible-playbook playbook.yml
```

## Workflow

```mermaid
graph LR
A[heat]
B(cloud-init)
D[configure ansible-pull as a cron.d]
Z[ansible]
x(prepare OS project)
Z --> x
Z --> A
A --> B
B --> C[install ansible git cron]
B --> D[configure ansible-pull as a cron.d]
D --> E[ansible-pull git pull and apply configuration]
E --> E
```

## Configuration

TBD

## Files and usage

### OpenStack heat

playbook.yml: This is the ansible playbook to deploy the heat stack
env.yml: This is the heat environment.
autoscale-vm.yaml: This is the heat template for the frontend web server.
scale-template.yml: This is the main heat template.

### Ansible virtual machines
local.yml: This is the ansible playbook call by ansible-pull by default
vm-config.yml: This is the ansible playbook configuring the frontend web.
sql-config.yml: This is the ansible playbook configuring the SQL Server.

## TODO

- Change playbook.yaml to be configurable by ansible vars
- Increase documentation
